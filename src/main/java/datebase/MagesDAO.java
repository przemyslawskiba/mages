package datebase;

import model.Mages;
import model.Spells;
import model.Wands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by pskiba on 08.07.2017.
 */
public class MagesDAO extends BaseDAO<Mages> {

    private String[] columns = {"name", "wand", "supervisor"};

    @Override
    public String getTableName() {
        return "mages";
    }

    @Override
    public Mages parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int wandId = result.getInt(3);
        int supervisorId = result.getInt(4);
        Wands wand = new WandsDAO().find(wandId);
        Mages supervisor = supervisorId == 0 ? null : new MagesDAO().find(supervisorId);
        List<Spells> spells = new SpellsDAO().getMagesSpells(id);
        return new Mages(name, wand, supervisor, spells);
    }

    @Override
    public Object[] getColumnsValues(Mages value) {
        Object[] values = {value.getName(), value.getWand(), value.getSupervisor()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Mages value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }


}
