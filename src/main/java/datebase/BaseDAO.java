package datebase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by pskiba on 07.07.2017.
 */
public abstract class BaseDAO<T> {

    public abstract String getTableName();

    public abstract T parseValue(ResultSet result) throws SQLException;

    public abstract Object[] getColumnsValues(T value);

    public abstract int getPrimaryKeyValue(T value);

    public abstract String[] getColumns();

    public static final Logger log = Logger.getLogger(BaseDAO.class.getName());

    private void setParams(PreparedStatement statement, Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            Object param = values[i];
            int paramIndex = i + 1;
            if (param instanceof String) {
                statement.setString(paramIndex, (String) param);
            } else if (param instanceof Integer) {
                statement.setInt(paramIndex, (Integer) param);
            } else {
                throw new IllegalArgumentException("Unsupported class " + param.getClass());
            }
        }
    }

    public void execute(String sql, Object[] params) {
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql.toString())) {
            setParams(statement, params);
            log.log(Level.INFO, "Executing query: " + statement.toString());
            statement.execute();
        } catch (SQLException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    public List<T> executeQuery(String sql, Object[] params) {
        List<T> values = new ArrayList<>();
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            setParams(statement, params);
            log.log(Level.INFO, "Executing query: " + statement.toString());
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    T value = parseValue(result);
                    values.add(value);
                }
            }
        } catch (SQLException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return values;
    }

    public void update(T value) {
        StringBuffer sql = new StringBuffer("UPDATE ");
        sql.append(getTableName()).append(" SET ");
        for (String column : getColumns()) {
            sql.append(column).append(" = ?, ");
        }
        sql.replace(sql.length() - 2, sql.length(), " ");
        sql.append("WHERE id = ?");
        Object[] params = getColumnsValues(value);
        params = Arrays.copyOf(params, params.length + 1);
        params[params.length - 1] = getPrimaryKeyValue(value);
        execute(sql.toString(), params);
    }

    public void insert(T value) {
        StringBuffer sql = new StringBuffer("INSERT INTO ");
        sql.append(getTableName()).append(" (");
        for (String column : getColumns()) {
            sql.append(column).append(", ");
        }
        sql.replace(sql.length() - 2, sql.length(), ")");
        sql.append(" VALUES (");
        for (int i = 0; i < getColumns().length; i++) {
            sql.append("?, ");
        }
        sql.replace(sql.length() - 2, sql.length(), ")");
        execute(sql.toString(), getColumnsValues(value));
    }


  /*  public void delete (int id) {
        String sql = "DELETE FROM" + getTableName() +  "WHERE id = ?";
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }*/

    public void delete(int id) {
        String sql = "DELETE FROM " + getTableName() + "  WHERE id = ?";
        Object[] params = {id};
        execute(sql, params);
    }



    /*public T find(int id) {
        String sql = "SELECT * FROM" + getTableName() + "WHERE id = ?";
        T value = null;
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                   value = parseValue(result);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return value;
    }*/

    public T find(int id) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        Object[] params = {id};
        return executeQuery(sql, params).get(0);
    }
}
