package datebase;

import model.Cores;
import model.Wands;
import model.Woods;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by pskiba on 09.07.2017.
 */
public class WandsDAO extends BaseDAO<Wands> {

    private String[] columns = {"wood", "core", "product_date"};

    @Override
    public String getTableName() {
        return "wands";
    }

    @Override
    public Wands parseValue(ResultSet result) throws SQLException {

        int woodId = result.getInt(2);
        Woods woods = new WoodsDAO().find(woodId);
        int coreId = result.getInt(3);
        Cores cores = new CoresDAO().find(coreId);
        Date productDate = result.getDate(4);
        return new Wands(woods, cores, productDate);
    }

    @Override
    public Object[] getColumnsValues(Wands value) {
        Object[] values = {value.getWood(), value.getCore(), value.getProductDate()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Wands value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

}
