package datebase;

import model.Cores;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by pskiba on 03.07.2017.
 */
public class CoresDAO extends BaseDAO<Cores> {

    private String[] columns = {"name", "power", "consistency"};

    @Override
    public String getTableName() {
        return "cores";
    }

    @Override
    public Cores parseValue(ResultSet result) throws SQLException {
        String name = result.getString(2);
        int power = result.getInt(3);
        int consistency = result.getInt(4);
        return new Cores(name, power, consistency);
    }

    @Override
    public Object[] getColumnsValues(Cores value) {
        Object[] values = {value.getName(), value.getPower(), value.getConsistency()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Cores value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }




    /*public void insert(Cores cores) {
        String sql = "INSERT INTO cores (name, power, consistency) VALUES (?, ?, ?)";
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, cores.getName());
            statement.setInt(2, cores.getPower());
            statement.setInt(3, cores.getConsistency());
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void update(Cores cores) {
        String sql = "UPDATE cores SET name = ?, power = ?, consistency = ? WHERE id = ?";
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, cores.getId());
            statement.setString(2, cores.getName());
            statement.setInt(3, cores.getPower());
            statement.setInt(4, cores.getConsistency());
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Cores find(int id) {
        Cores cores = find(id);
        return cores;
    }

    public void delete(Cores cores) {
        delete(cores.getId());
    }*/
}


