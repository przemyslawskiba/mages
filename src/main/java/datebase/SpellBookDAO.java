package datebase;

import model.SpellBook;
import model.Spells;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by pskiba on 08.07.2017.
 */
public class SpellBookDAO extends BaseDAO<SpellBook> {

    private String[] columns = {"title", "author", "publish_date"};

    @Override
    public String getTableName() {
        return "spells_book";
    }

    @Override
    public SpellBook parseValue(ResultSet result) throws SQLException {
        int Id = result.getInt(1);
        String title = result.getString(2);
        String author = result.getString(3);
        Date publishYear = result.getDate(4);
        List<Spells> spells = new SpellsDAO().getSpellsBooks(Id);
        return new SpellBook(title, author, publishYear, spells);
    }

    public Object[] getColumnsValues(SpellBook value) {
        Object[] values = {value.getTitle(), value.getAuthor(), value.getPublishDate(), value.getSpells()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(SpellBook value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }


}
