import manager.*;

import java.text.ParseException;
import java.util.Scanner;

/**
 * Created by pskiba on 09.07.2017.
 */
public class Manager {

    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);
        String command;
        CoresManager coresManager = new CoresManager();
        MagesManager magesManager = new MagesManager();
        SpellBookManager spellBookManager = new SpellBookManager();
        SpellsManager spellsManager = new SpellsManager();
        WandsManager wandsManager = new WandsManager();
        WoodsManager woodsManager = new WoodsManager();

        boolean run = true;

        while (run) {
            System.out.print("command: ");
            command = scanner.next();

            switch (command) {
                case "cores": {
                    coresManager.manage(scanner);
                    break;
                }
                case "mages": {
                    magesManager.manage(scanner);
                    break;
                }
                case "spellbook": {
                    spellBookManager.manage(scanner);
                    break;
                }
                case "spells": {
                    spellsManager.manage(scanner);
                    break;
                }
                case "wands": {
                    wandsManager.manage(scanner);
                    break;
                }
                case "woods": {
                    woodsManager.manage(scanner);
                    break;
                }
                case "quit": {
                    run = false;
                    break;
                }
            }
        }
    }
}
