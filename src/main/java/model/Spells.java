package model;

import lombok.*;

/**
 * Created by pskiba on 03.07.2017.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Spells {

    private int id;
    private String incantation;

    public Spells(String incantation) {
        this.incantation = incantation;
    }
}
