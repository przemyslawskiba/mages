package model;

import lombok.*;

/**
 * Created by pskiba on 03.07.2017.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Woods {

    private int id;

    private String name;

    private int toughness;

    public Woods(String name, int toughness) {
        this.name = name;
        this.toughness = toughness;
    }
}
