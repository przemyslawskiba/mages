package model;

import lombok.*;

/**
 * Created by pskiba on 03.07.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString


public class Cores {

    private int id;

    private String name;

    private int power;

    private int consistency;

    public Cores(String name, int power, int consistency) {
        this.name = name;
        this.power = power;
        this.consistency = consistency;
    }
}
