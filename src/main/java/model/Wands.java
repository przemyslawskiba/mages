package model;

import lombok.*;

import java.util.Date;


/**
 * Created by pskiba on 03.07.2017.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Wands {

    private int id;

    private Woods wood;

    private Cores core;

    private Date productDate;

    public Wands(Woods wood, Cores core, Date productDate) {
        this.wood = wood;
        this.core = core;
        this.productDate = productDate;
    }
}
