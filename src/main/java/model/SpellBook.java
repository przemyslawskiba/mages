package model;

import lombok.*;

import java.util.Date;
import java.util.List;


/**
 * Created by pskiba on 03.07.2017.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class SpellBook {

    private int id;

    private String title;

    private String author;

    private Date publishDate;

    private List<Spells> spells;

    public SpellBook(String title, String author, Date publishDate, List<Spells> spells) {
        this.title = title;
        this.author = author;
        this.publishDate = publishDate;
        this.spells = spells;
    }
}
