package manager;

import datebase.SpellBookDAO;
import datebase.SpellsDAO;
import model.SpellBook;
import model.Spells;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

/**
 * Created by pskiba on 09.07.2017.
 */
public class SpellBookManager extends BaseManager<SpellBook, SpellBookDAO> {


    public SpellBookManager() {
        dao = new SpellBookDAO();
    }

    @Override
    protected SpellBook parseNew(Scanner scanner) throws ParseException {
        System.out.print("new title: ");
        String title = scanner.next();
        System.out.print("new author: ");
        String author = scanner.next();
        System.out.print("new publish date: ");
        String publishDateString = scanner.next();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        java.util.Date date = formatter.parse(publishDateString);
        System.out.println("new incantation");
        String incantation = scanner.next();
        Spells spells = new Spells(incantation);
        List<Spells> spells1 = new SpellsDAO().createSpellList(spells);
        return new SpellBook(title, author, date, spells1);
    }

    @Override
    protected void copyId(SpellBook from, SpellBook to) {
        to.setId(from.getId());
    }
}
