package manager;

import datebase.*;
import model.*;

import java.util.List;
import java.util.Scanner;

/**
 * Created by pskiba on 09.07.2017.
 */
public class MagesManager extends BaseManager<Mages, MagesDAO> {

    public MagesManager() {
        dao = new MagesDAO();
    }

    @Override
    protected Mages parseNew(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new ID wand: ");
        int wand = scanner.nextInt();
        System.out.println("new ID supervisor");
        int supervisorId = scanner.nextInt();
        System.out.println("new ID book of spells");
        int spellId = scanner.nextInt();
        Wands wands = new WandsDAO().find(wand);
        Mages supervisor = new MagesDAO().find(supervisorId);
        List<Spells> spells = new SpellsDAO().getSpellsBooks(spellId);
        return new Mages(name, wands, supervisor, spells);
    }

    @Override
    protected void copyId(Mages from, Mages to) {
        to.setId(from.getId());
    }

}
