package manager;

import datebase.WoodsDAO;
import model.Woods;

import java.util.Scanner;

/**
 * Created by pskiba on 09.07.2017.
 */
public class WoodsManager extends BaseManager<Woods, WoodsDAO> {

    public WoodsManager() {
        dao = new WoodsDAO();
    }

    @Override
    protected Woods parseNew(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new toughness: ");
        int toughness = scanner.nextInt();
        return new Woods(name, toughness);
    }

    @Override
    protected void copyId(Woods from, Woods to) {
        to.setId(from.getId());
    }
}
