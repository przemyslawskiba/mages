package manager;

import datebase.WandsDAO;
import datebase.WoodsDAO;
import datebase.CoresDAO;
import model.Cores;
import model.Wands;
import model.Woods;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Scanner;

/**
 * Created by pskiba on 09.07.2017.
 */
public class WandsManager extends BaseManager<Wands, WandsDAO> {

    public WandsManager() {
        dao = new WandsDAO();
    }

    @Override
    protected Wands parseNew(Scanner scanner) throws ParseException {
        System.out.print("new ID wood: ");
        int wood = scanner.nextInt();
        System.out.print("new ID core: ");
        int core = scanner.nextInt();
        System.out.println("new product date");
        String dateString = scanner.next();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy MMM dd");
        Date date = formatter.parse(dateString);

        Woods woods = new WoodsDAO().find(wood);
        Cores cores = new CoresDAO().find(core);
        return new Wands(woods, cores, date);
    }

    @Override
    protected void copyId(Wands from, Wands to) {
        to.setId(from.getId());
    }
}
