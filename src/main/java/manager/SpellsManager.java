package manager;

import datebase.SpellsDAO;
import model.Spells;

import java.util.Scanner;

/**
 * Created by pskiba on 09.07.2017.
 */
public class SpellsManager extends BaseManager<Spells, SpellsDAO> {


    public SpellsManager() {
        dao = new SpellsDAO();
    }

    @Override
    protected Spells parseNew(Scanner scanner) {
        System.out.print("new incantation: ");
        String incantation = scanner.next();
        return new Spells(incantation);
    }

    @Override
    protected void copyId(Spells from, Spells to) {
        to.setId(from.getId());
    }
}
