package manager;

import datebase.CoresDAO;
import model.Cores;

import java.util.Scanner;

/**
 * Created by pskiba on 07.07.2017.
 */
public class CoresManager extends BaseManager<Cores, CoresDAO> {


    public CoresManager() {
        dao = new CoresDAO();
    }

    @Override
    protected Cores parseNew(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new power: ");
        int power = scanner.nextInt();
        System.out.print("new consistency: ");
        int consistency = scanner.nextInt();
        return new Cores(name, power, consistency);
    }

    @Override
    protected void copyId(Cores from, Cores to) {
        to.setId(from.getId());
    }

  /*  public static void manageCores (Scanner scanner) {
        System.out.print("command: ");
        String command = scanner.next();
        switch (command) {
            case "add": {
                add(scanner);
                break;
            }
            case "find": {
                find(scanner);
                break;
            }
            case "delete": {
                delete(scanner);
                break;
            }
            case "update": {
                update(scanner);
                break;
            }
        }
    }*/



   /*
    private static void update(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        CoresDAO dao = new CoresDAO();
        Cores cores = dao.find(id);
        System.out.println(cores);
        if (cores != null) {
            System.out.print("new name: ");
            String name = scanner.next();
            cores.setName(name);
            System.out.print("new power: ");
            int power = scanner.nextInt();
            cores.setPower(power);
            System.out.print("new consistency: ");
            int consistency = scanner.nextInt();
            cores.setConsistency(consistency);
            dao.update(cores);
        }
    }

    private static void delete(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        CoresDAO dao = new CoresDAO();
        dao.delete(dao.find(id));
    }

    private static void find(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        CoresDAO dao = new CoresDAO();
        Cores cores = dao.find(id);
        System.out.println(cores);
    }

    private static void add(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        System.out.print("power: ");
        int power = scanner.nextInt();
        System.out.println("consistency: ");
        int consistency = scanner.nextInt();
        CoresDAO dao = new CoresDAO();
        Cores cores= dao.create(name, power,consistency);
        dao.insert(cores);
    }*/
}

